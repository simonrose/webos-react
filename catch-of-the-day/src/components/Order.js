import React from "react";
import { formatPrice } from "../helpers";

class Order extends React.Component {
  renderOrder = (key) => {
    const fish = this.props.fishes[key];
    const amount = this.props.order[key];
    if (fish.status === "available") {
      return (
        <li key={key}>
          {amount} lbs {fish.name}
          {formatPrice(amount * fish.price)}
        </li>
      );
    } else {
      return <li key={key}>I got no {fish ? fish.name : "nofish"} for you</li>;
    }
  };

  render() {
    const orderIds = Object.keys(this.props.order);
    const total = orderIds.reduce((prev, key) => {
      const fish = this.props.fishes[key];
      const amount = this.props.order[key];
      const isAvailable = fish && fish.status === "available";
      return prev + (isAvailable ? fish.price * amount : 0);
    }, 0);
    return (
      <div className="order-wrap">
        <h2>Ordder</h2>
        <ul className="order">{orderIds.map(this.renderOrder)}</ul>
        <div className="total">
          Total:
          <strong>{formatPrice(total)}</strong>
        </div>
      </div>
    );
  }
}

export default Order;
